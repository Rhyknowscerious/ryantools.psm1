function Get-SelectionFromMenu(

    [Parameter(
        Mandatory=$true
    )]
    [string]
    $Message,

    
    [Parameter(
        Mandatory=$true
    )]
    [string[]]
    $Options
) {
    <#
    .SYNOPSIS
    Create an easy menu for interactive PowerShell scripts

    .DESCRIPTION
    Get-SelectionFromMenu was created as an alternative to New-EasyMenu for 
    versions of PowerShell which do not support the EasyMenu module.
    
    .PARAMETER Message
    Enter a message for users to read before showing options.

    .PARAMETER Options
    Enter an array of options which will be numbered for the user among which 
    to choose.

    .EXAMPLE
    
    Get-SelectionFromMenu -Message "What is your favorite color?" -Options @("Red","Yellow","Blue")
    
    ...That is the basic syntax

    .EXAMPLE
    
    switch( `
        Get-SelectionFromMenu `
            -Message "What would you rather eat?" `
            -Options @( `
                "pizza", `
                "cheesecake" `
            ) `
    ) { `
        1 { `
            Write-Host "I like pizza too."; `
            Break `
        } `
        2 { `
            Write-Host "Oh really? I'd rather eat cheesecake"; `
            Break `
        } `
        Default { `
            Write-Host "Invalid selection"; `
            Break `
        } `
    }

    ...As you can see, Get-SelectionFromMenu works very well in a switch 
    statement:

    .INPUTS
    None

    .OUTPUTS
    System.Int32 This function should return an integer based on the user's 
    selection.

    .LINK 
    Find more of my software here: https://Rhyknowscerious.bitbucket.com

    .LINK
    Learn more about PowerShell modules and how to use them here: https://activedirectorypro.com/install-powershell-modules/

    .NOTES
    To install modules for a user, save modules here:
    
        C:\Users\userprofile\Documents\WindowsPowerShell\Modules\<PutMolduleFolderHere>

    To install modules for all users, save modules here

        C:\Program Files\WindowsPowerShell\Modules\<PutModuleFolderHere>

    #>

    $_nl = [System.Environment]::NewLine

    Write-Host $Message$_nl

    $_optionCount = 0
    $Options | % {

        $_optionCount ++
        Write-Host "$_optionCount) $_"

    }

    $_output = Read-Host "$_nl`Pick a number"

    return $_output
}