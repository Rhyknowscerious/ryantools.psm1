function Get-Brightness {

    $_Brightness = Get-WmiObject -Namespace root/WMI -Class WmiMonitorBrightness -Verbose

    $_Brightness.CurrentBrightness

}

function Get-PowerScheme {
<#
    
    .SYNOPSIS
    Get a list of Windows Power Schemes on your system

    .DESCRIPTION
    This is a wrapper for POWERCFG.EXE /GETACTIVESHCEME and POWERCFG.EXE /LIST

    .PARAMETER Active
    Shows the active Power Scheme. Omit this to see all Power Schemes on your system.
    
    .EXAMPLE
    Get-PowerScheme
    
    .EXAMPLE
    Get-PowerScheme -Active
#>

Param(
    [Alias('Current')]
    [switch]$Active
)

Begin {}

Process {

    if ($Active.IsPresent) {
        
        
        $_powerCfgRawOutput = (POWERCFG.EXE /GETACTIVESCHEME)

        $_powerCfgOutput = $_powerCfgRawOutput -split ': '

        $_powerCfgOutput = $_powerCfgOutput[1]

        $_output = New-Object -TypeName psobject
        $_output | Add-Member -MemberType NoteProperty -Name "SchemeName" -Value $_powerCfgOutput.split('()')[1]
        $_output | Add-Member -MemberType NoteProperty -Name "SchemeGuid" -Value ($_powerCfgOutput -split ' ')[0]

    } else {

        $_powerCfgRawOutput = (POWERCFG.EXE /LIST)

        $_powerCfgOutput = ($_powerCfgRawOutput | select -skip 3)

        $_powerCfgOutput = $_powerCfgOutput -replace '^.+GUID: ',''

        [psobject[]]$_outputArray = @()

        $_powerCfgOutput | % {

            $_output = New-Object -TypeName psobject
            $_output | Add-Member -MemberType NoteProperty -Name "SchemeName" -Value $_.split('()')[1]
            $_output | Add-Member -MemberType NoteProperty -Name "SchemeGuid" -Value ($_ -split ' ')[0]

            $_outputArray += $_output
        }

        $_output = $_outputArray
    }

    return $_output
}


End {}
}

function Get-PowerSchemeSetting{
<#
    
    .SYNOPSIS
    Gets settings of a Windows Power Scheme

    .DESCRIPTION
    This is a wrapper for "POWERCFG.EXE /QUERY". If supplied no arguments, 
    output defaults to the current power scheme.

    .PARAMETER SchemeGuid
    Enter a Windows Power Scheme GUID here.

    .PARAMETER SubGuid
    
    Enter a Windows Power Setting Subgroup GUID here.

    .EXAMPLE
    Get-PowerSchemeSetting
#>

Param(
    [Alias(
        'PowerScheme''Scheme'
    )]
        $SchemeGuid,

    [Alias(
        'Sub','Subgroup','SubgroupSetting'
    )]
        $SubGuid
)

Begin {}

Process {

    
    POWERCFG.EXE /QUERY $SchemeGuid $SubGuid

}

End {}
}

function Set-Brightness {

    Param(
        [int16]$BrightnessLevel
    )

    if (

        $BrightnessLevel -lt 0 -or 
        $BrightnessLevel -gt 100

    ) {
        Write-Error "Brightness must be set from 0 to 100."
        return
    }

    $_BrightnessMethods = Get-WmiObject -Namespace root/WMI -Class WmiMonitorBrightnessMethods -Verbose
    
    $_BrightnessMethods.WmiSetBrightness(1,$BrightnessLevel)

}

function Set-PowerSchemeSetting {
<#
    
    .SYNOPSIS
    Sets (changes) a Windows Power Scheme setting value.

    .DESCRIPTION
    This is a wrapper for POWERCFG.EXE /CHANGE.

    .PARAMETER SchemeSettingName
    This has to be one of the following:

    monitor-timeout-ac
    monitor-timeout-dc
    disk-timeout-ac
    disk-timeout-dc
    standby-timeout-ac
    standby-timeout-dc
    hibernate-timeout-ac
    hibernate-timeout-dc

    .PARAMETER Minutes
    Enter an amount of time in minutes as a positive integer

    .EXAMPLE
    Set-PowerSchemeSetting -SchemeSettingName 'monitor-timeout-ac' -Minutes 3
#>

Param(
    [Parameter(
        Mandatory='True',
        HelpMessage=
            'These are the valid arguments: ' + 
            'Monitor-Timeout-Ac, '+
            'Monitor-Timeout-Dc, '+
            'Disk-Timeout-Ac, '+
            'Disk-Timeout-Dc, '+
            'Standby-Timeout-Ac, '+
            'Standby-Timeout-Dc, '+
            'Hibernate-Timeout-Ac, '+
            'Hibernate-Timeout-Dc'
    )]
    [Alias(
        'SchemeSetting','Setting'
    )]
    [ValidateSet(
        'Monitor-Timeout-Ac',
        'Monitor-Timeout-Dc',
        'Disk-Timeout-Ac',
        'Disk-Timeout-Dc',
        'Standby-Timeout-Ac',
        'Standby-Timeout-Dc',
        'Hibernate-Timeout-Ac',
        'Hibernate-Timeout-Dc'
    )]
        $SchemeSettingName,
    
    [Parameter(
        Mandatory=$true,
        HelpMessage='Enter an amount of minutes:'
    )]
        [uint16]
            $Minutes
)

Begin {}

Process {
    
    
    POWERCFG /CHANGE $SchemeSettingName $Minutes
    
}

End {}
}

function Set-PowerScheme {
<#
    
    .SYNOPSIS
    Sets the active power scheme.

    .DESCRIPTION
    This is a wrapper for POWERCFG.EXE /SETACTIVE.

    .PARAMETER SchemeGuid
    This should be a Windows Power Scheme GUID.

    .EXAMPLE
    Set-PowerScheme -SchemeGuid 381b4222-f694-41f0-9685-ff5bb260df2e

    .EXAMPLE
    Set-PowerScheme -Interactive
#>

[CmdletBinding(
    DefaultParameterSetName = 'non-interactive'
)]

Param(
    [Parameter(
        ParameterSetName = 'non-interactive',
        Mandatory=$true
    )]
        $SchemeGuid,

    [Parameter(
        ParameterSetName = 'interactive',
        Mandatory=$true
    )]
    [Switch]
    $Interactive
)

Begin {
    if ($Interactive.IsPresent) {

        $_powerShcemes = Get-PowerScheme | sort SchemeName
    }
}

Process {

    if ($Interactive.IsPresent) {

        [uint16]$_choice = -1 + (

            Get-SelectionFromMenu `
                -Message "The following power schemes were found on your system:" `
                -Options $_powerShcemes.SchemeName

        )

        if ($_choice -lt 0 -or $_choice -gt $_powerShcemes.length-1) {

            throw {"Invalid Selection"}

        }

        $_selectedPowerScheme = $_powerShcemes[$_choice]

        $SchemeGuid = $_selectedPowerScheme.SchemeGuid

        Write-Host "You chose this power scheme:"

        $_selectedPowerScheme | Format-List

    } 

    POWERCFG.EXE /SETACTIVE $SchemeGuid 

}

End {}
}