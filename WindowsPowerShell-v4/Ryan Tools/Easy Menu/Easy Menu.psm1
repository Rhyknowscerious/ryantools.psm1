class EasyMenu
{
    [String]$Title
    [String]$Prompt
    [String[]]$Options #Simulated Enum
    hidden [String]$Message
    hidden [Bool]$Open
    [String]$Selection
    hidden [ScriptBlock]$ExecuteFunction

    EasyMenu(
        [String]$_title,
        [String]$_prompt,
        [String[]]$_options,
        [ScriptBlock]$_functionReference
    ){
        $this.Title = $_title
        $this.Prompt = $_prompt
        $this.Options = $_options
        $this.Selection = $null
        $this.ExecuteFunction = $_functionReference

        If ($_options.Length -gt 0) {
            $this.Message = ""
            For ($i = 0; $i -lt $_options.Length; $i++) {
                If ($i -ne 0) {$this.Message += "$([Environment]::NewLine)"}
                $this.Message += "$($i + 1)`) $($_options[$i])"
            }
        } else {
            $this.Message = $null
        }

        $this.Open = $true
    }

    hidden [Void]Show(){
        
        If ($this.Title -ne $null -and $this.Title -ne "") {
            Write-Host "$($this.Title):"
        }

        If ($this.Prompt -ne $null -and $this.Prompt -ne "") {
            Write-Host "$($this.Prompt)"
        }

        If ($this.Message -ne $null -and $this.Message -ne "") {
            Write-Host "$($this.Message)"
        }
    }

    hidden [Void]Select() {
        $this.Show()
        If ($this.Options.Length -gt 0) {

            $this.Selection = Read-Host "Pick a number"
            Write-Host "$([Environment]::NewLine)" -NoNewline
        
        } ElseIf (

            $this.Options.Length -eq 0 -and 
            $this.Prompt -ne ""

        ){
        
            $this.Selection = Read-Host
            Write-Host "$([Environment]::NewLine)" -NoNewline
            
        }
    }

    [Void]Execute(){
        $this.Select()
        $this.ExecuteFunction.Invoke()
    }

    [Void]ShowStatus(){
        Write-Host "Menu Status:"
        Write-Host "Title: $($this.Title)"
        Write-Host "Open: $($this.Open)"
        Write-Host "Options: " -NoNewline
        For ($i = 0; $i -lt $this.Options.Length; $i++) {
            If ($i -eq 0) {Write-Host "`@`(" -NoNewline}
            Write-Host "$($this.Options[$i])" -NoNewline
            If ($i -ne ($this.Options.Length -1)) {Write-Host "," -NoNewline}
            If ($i -eq ($this.Options.Length - 1)) {Write-Host ")"}
        }
        Write-Host "Message: `"$($this.Message)`""
    }
}

function New-EasyMenu(
    [String]$Title,
    [String]$Prompt,
    [String[]]$Options,
    [ScriptBlock]$ScriptBlock
){
    [EasyMenu]::New($Title,$Prompt,$Options,$ScriptBlock)
}