function Invoke-AnyConnectVpnCli {
    <#
        .SYNOPSIS
        Invokes VPNCLI.EXE from Cisco AnyConnect for Windows.
        
        .DESCRIPTION
        This is a wrapper for VPNCLI.EXE from Cisco's AnyConnect VPN client for Windows.
    #>
    
    Param(

        [Parameter(ParameterSetName = "Option Help")]
        [Alias("h")]
        [switch]
        $Help,

        [Parameter(ParameterSetName = "Option Version")]
        [Alias("v")]
        [switch]
        $Version,

        [Parameter(ParameterSetName = "Option Standard Input")]
        [Alias("s","read")]
        [switch]
        $ReadFromStandardInput,

        [Parameter(ParameterSetName = "Command Connect")]
        [switch]
        $Connect,

        [Parameter(ParameterSetName = "Command Disconnect")]
        [switch]
        $Disconnect,

        [Parameter(ParameterSetName = "Command Hosts")]
        [switch]
        $Hosts,
        
        [Parameter(ParameterSetName = "Command State")]
        [switch]
        $State,
        
        [Parameter(ParameterSetName = "Command Stats")]
        [switch]
        $Stats,
        
        [Parameter(ParameterSetName = "Command Connect", Position = 1)]
        [Parameter(ParameterSetName = "Command Disconnect", Position = 1)]
        [Parameter(ParameterSetName = "Command Hosts", Position = 1)]
        [Parameter(ParameterSetName = "Command State", Position = 1)]
        [Parameter(ParameterSetName = "Command Stats", Position = 1)]
        [Alias("Host")]
        $ComputerName

    )
    
    Begin{

        $_nl = [System.Environment]::NewLine

        $_vpnCli = "C:\Program Files (x86)\Cisco\Cisco AnyConnect Secure Mobility Client\vpncli.exe"

        if ((Test-Path $_vpnCli)) {
            
            Write-Verbose "Found Cisco AnyConnect installation at `"$_vpnCli`""

        } else {
            
            Write-Error "Cisco AnyConnect installation not found at `"$_vpnCli`""
            
            return

        }

        $Arguments = ""
        
        if ($Help.IsPresent) {$Arguments += "-h"}

        if ($Version.IsPresent) {$Arguments += "-v"}

        if ($ReadFromStandardInput.IsPresent) {$Arguments += "-s"}

        if ($Connect.IsPresent) {$Arguments += "connect"}

        if ($Disconnect.IsPresent) {$Arguments += "disconnect"}

        if ($Hosts.IsPresent) {$Arguments += "hosts"}

        if ($State.IsPresent) {$Arguments += "state"}

        if ($Stats.IsPresent) {$Arguments += "stats"}

    }

    Process{
        
        $Message = "$_nl`Using command `"$_vpnCli $Arguments $ComputerName`""

        if ($Message[-2] -eq " ") {
            $Message = $Message.Substring(0,($Message.Length-2)) + "`"$_nl"
        } else {
            $Message += "$_nl"
        }

        Write-Host $Message

        & $_vpnCli $Arguments $ComputerName | ? {$_}

    }

    End{}
    
}

function Invoke-TightVncViewer {
    <#
        
        .SYNOPSIS
        Starts a Tight Vnc Viewer (vnc client) session

        .DESCRIPTION
        This is a PowerShell wrapper for TVNVIEWER.EXE - Tight VNC's VNC client software.

        .PARAMETER ComputerName
        Enter an array of IP addresses or hostnames of computers running TVNSERVER.EXE (Tight VNC's VNC server software) to which you'd like to connect.

        .PARAMETER Port
        Enter the TCP port on which the machine running TVNSERVER.EXE (Tight VNC's VNC server software) is listening.

        .PARAMETER PromptForPassword
        Enter the Tight VNC password configured on the server.

        .PARAMETER OptionsFile
        Enter the path to the Tight VNC options file.

        .PARAMETER Help
        Use this switch to invoke "TVNVIEWER.EXE -help"

        .INPUTS
        None

        .OUTPUTS
        None
    #>

    [CmdletBinding(
        DefaultParameterSetName="Default"
    )]

    Param(

        [Parameter(
            ParameterSetName="Default"
        )]
        $ComputerName,
        
        [Parameter(
            ParameterSetName="Default"
        )]
        [uint16]$Port,
        
        [Parameter(
            ParameterSetName="Default"
        )]
        [switch]$PromptForPassword,
        
        [Parameter(
            ParameterSetName="Default"
        )]
        [uint16]$InvocationLimit = 10,
        
        [Parameter(
            ParameterSetName="OptionsFile",
            Mandatory=$true
        )]
        $OptionsFile,

        [Parameter(
            ParameterSetName="Help",
            Mandatory=$true
        )]
        [switch]$Help
    )
    
    Begin{
        
        $_nl = [System.Environment]::NewLine

        if ($ComputerName.GetType() -eq [object[]] -and $ComputerName.length -gt $InvocationLimit) {
            
            $_connectionCount = $ComputerName.length
                
            $_message = 
                "You're trying to start connections to $_connectionCount " + 
                "computers and the limit of connections is " + 
                "$InvocationLimit. To fix this, connect to less computers " + 
                "or change the -InvocationLimit parameter."

            Write-Error $_message

            $_initializationFailed = $true

            return
        }

        Write-Verbose "Invocation Limit is $InvocationLimit."

        $_tightVncViewer = 'C:\Program Files\TightVNC\tvnviewer.exe'

        if ($PromptForPassword.IsPresent) {

            $_credential = 
                "-password=" + 
                (Get-Credential "TightVncViewer").GetNetworkCredential().Password

            $_hiddenPassword = '-password=******'

        }

        if ($Port -and $Port -ne 0) {

                $Port = "-port=$Port"

        } else {
            
            Remove-Variable Port

        }

        if ($OptionsFile) {$OptionsFile = "-optionsfile=$OptionsFile"}
    }

    Process {
        
        if ($_initializationFailed) {return}

        if ($ComputerName) {
            
            $Computername | % {
            
                Write-Verbose "Using command: `"$_tightVncViewer -host=$_ $Port $_hiddenPassword`"$_nl"

                & $_tightVncViewer "-host=$_" $Port $_credential

            }

        } elseif ($OptionsFile) {
            
            Write-Verbose "Using command: `"$_tightVncViewer $OptionsFile`"$_nl"

            & $_tightVncViewer $OptionsFile

        } elseif ($Help.IsPresent) {
            
            Write-Verbose "Using command: `"$_tightVncViewer -Help`"$_nl"

            & $_tightVncViewer -Help

        } else {
            
            Write-Verbose "Using command: `"$_tightVncViewer $Port`"$_nl"

            & $_tightVncViewer $Port
            
        }
    }
}

function Invoke-WindowsRdpSession {
<#
    
    .SYNOPSIS
    Wrapper for mstsc.exe

    .DESCRIPTION
    This command basically follows mstsc.exe exactly. If you'd like to see the original help content for the mstsc.exe program, use the -MstscHelp parameter on this command.

    .NOTES
    Use the -MstscHelp switch on this command to get more help.

    .EXAMPLE
    New-RemoteDesktopSession -Help

    .EXAMPLE
    New-RemoteDesktopSession -Server HostnameOrIpAddress

    .Example
    New-RemoteDesktopSession -Path 'C:\Default.rdp'

    .Example
    New-RemoteDesktopSession 'C:\Default.rdp' -Edit

#>

[CmdletBinding(
    DefaultParameterSetName = 'ConnectionFile'
)]

Param(

    [Parameter(
        ParameterSetName = "ConnectionFile",
        Position = 0
    )]
    [Parameter(
        ParameterSetName = "Edit",
        Mandatory = $true,
        Position = 0
    )]
    [ValidateNotNullOrEmpty()]
    [Alias('Path')]
    $ConnectionFilePath,

    [Parameter(
        ParameterSetName = "Edit",
        Mandatory = $true
    )]
    [switch]
    $Edit,

    [Parameter(
        ParameterSetName = "NoConnectionFile",
        Mandatory = $true,
        Position = 0
    )]
    [ValidateNotNullOrEmpty()]
    [Alias('v,ComputerName')]
    $Server,

    [Parameter(
        ParameterSetName = "NoConnectionFile",
        Position = 1
    )]
    [ValidateNotNullOrEmpty()]
    $Port,

    [Parameter(
        ParameterSetName = "NoConnectionFile",
        Position = 2
    )]
    [ValidateNotNullOrEmpty()]
    [Alias('g')]
    $Gateway,

    [Parameter(
        ParameterSetName = "NoConnectionFile"
    )]
    [switch]
    $Admin,

    [Parameter(
        ParameterSetName = "NoConnectionFile"
    )]
    [Alias('f')]
    [switch]
    $FullScreen,

    [Parameter(
        ParameterSetName = "NoConnectionFile"
    )]
    [Alias('w')]
    $Width,
    
    [Parameter(
        ParameterSetName = "NoConnectionFile"
    )]
    [Alias('h')]
    $Height,
    
    [Parameter(
        ParameterSetName = "NoConnectionFile"
    )]
    [switch]
    $Public,
    
    [Parameter(
        ParameterSetName = "NoConnectionFile"
    )]
    [switch]
    $Span,
    
    [Parameter(
        ParameterSetName = "NoConnectionFile"
    )]
    [switch]
    $MultiMon,
    
    [Parameter(
        ParameterSetName = "NoConnectionFile"
    )]
    [switch]
    $RestrictedAdmin,
    
    [Parameter(
        ParameterSetName = "NoConnectionFile"
    )]
    [switch]
    $RemoteGuard,
    
    [Parameter(
        ParameterSetName = "NoConnectionFile"
    )]
    [switch]$Prompt,
    
    [Parameter(
        ParameterSetName = "NoConnectionFile"
    )]
    [Alias('SessionId')]
    $Shadow,
    
    [Parameter(
        ParameterSetName = "NoConnectionFile"
    )]
    [switch]
    $Control,
    
    [Parameter(
        ParameterSetName = "NoConnectionFile"
    )]
    [switch]
    $NoConsentPrompt,
    
    [Parameter(
        ParameterSetName = "Help",
        Mandatory = $true
    )]
    [Alias('Help')]
    [switch]
    $MstscHelp

)

Begin {
    $_mstscCommand = 'mstsc'

    [string[]]$_mstscArguments = @()

    if ($Edit) {$_mstscArguments += '/edit'}

    if ($ConnectionFilePath) {$_mstscArguments += "$ConnectionFilePath"}

    if ($Server) {

        $_serverArgument = "/v:$Server"
        if ($Port) {
            $_serverArgument += ":$Port"
        }

        $_mstscArguments += $_serverArgument

    }

    if ($Gateway) {$_mstscArguments += "/g:$Gateway"}

    if ($Admin) {$_mstscArguments += '/admin'}

    if ($FullScreen) {$_mstscArguments += '/f'}

    if ($Width) {$_mstscArguments += "/w:$Width"}

    if ($Height) {$_mstscArguments += "/h:$Height"}

    if ($Public) {$_mstscArguments += '/public'}

    if ($Span) {$_mstscArguments += '/span'}

    if ($MultiMon) {$_mstscArguments += '/multimon'}

    if ($RestrictedAdmin) {$_mstscArguments += '/restrictedAdmin'}

    if ($RemoteGuard) {$_mstscArguments += '/remoteGuard'}

    if ($Prompt) {$_mstscArguments += '/prompt'}

    if ($RemoteGuard) {$_mstscArguments += '/remoteGuard'}

    if ($Shadow) {$_mstscArguments += "/shadow:$Shadow"}

    if ($Control) {$_mstscArguments += '/control'}

    if ($NoConsentPrompt) {$_mstscArguments += '/noConsentPrompt'}

    if ($MstscHelp) {$_mstscArguments += '/?'}
}

Process {
    $_statusMessage = "Using command `"$_mstscCommand"
    if ($_mstscArguments.Length -gt 0) {$_statusMessage += ' '}
    $_statusMessage += "$_mstscArguments`""

    Write-Host $_statusMessage
    & $_mstscCommand $_mstscArguments

}

End {}
}