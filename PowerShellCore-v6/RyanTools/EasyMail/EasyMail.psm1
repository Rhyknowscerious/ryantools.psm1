# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# Copyright 2019 Ryan James Decker

function Send-EasyMail {
    <#
        .SYNOPSIS
        This is a wrapper for sending email via .NET classes.

        .DESCRIPTION
        I feel that the cmdlet Send-MailMessage is too simplified and the .NET 
        classes is a hassle to use evertime you want to send an email through 
        PowerShell.
    
        This wrapper utilizes the following .NET classes to send an email:

        - System.Net.Mail.SmtpClient
        - System.Net.NetworkCredential
        - System.Net.Mail.MailMessage

        .PARAMETER MailServer
        Alias: -Server

        Enter the address of your mail server here like 'snmp.gmail.com' or '192.168.0.33'
        
        .PARAMETER MailServerPort
        Alias: -Port
        
        Enter the listening port of the mail server, like 25, 587, etcetra.

        .PARAMETER EnableSsl
        Alias: -SSL
        
        Add this switch to enable SSL. Omit otherwise.

        .PARAMETER SmtpCredentials
        Aliases: -User -UserName -LogIn -Credential
        
        Use SmtpCredentials to authenticate yourself to your mail server. Enter a 
        PsCredential as an argument. See examples for details.

        .PARAMETER SenderEmailAddress
        Aliases: -Sender -From
        
        SenderEmailAddress requires a string but can be formatted to separate a 
        human-friendly name from an email address by using name <address> 
        syntax.

        .PARAMETER ReplyToAddress
        Alias: -ReplyTo
        
        Add a reply address different from the sender address. ReplyToAddress 
        supports name <address> syntax.
    
        .PARAMETER VisibleRecipientEmailAddresses
        Aliases: -Recipients -To

        This is your typical TO email field. Supports name <address> syntax.

        .PARAMETER VisibleCarbonCopyEmailAddresses
        Alias: -CC

        This is your typical CC email field. Supports name <address> syntax.

        .PARAMETER BlindCarbonCopyEmailAddresses
        Alias: -BCC

        This is your typical BCC email field. Supports name <address> syntax.

        .PARAMETER Subject
        Your typical Subject email field.

        .PARAMETER Body
        Alias: -Message

        This is the main part of your email.

        .PARAMETER Attachments

        Attachments accepts an array of strings. These strings should be 
        paths to files you'd like to attach to your email.

        .PARAMETER Priority
        Priority needs to be an integer because it's .NET class is an Enumeration.
        This wrapper will take a string and convert it into an integer.
        Read more here: https://docs.microsoft.com/en-us/dotnet/api/system.net.mail.mailpriority?view=netframework-4.7.2

    #>

    <#
    [CmdletBinding(
        SupportsShouldProcess=$true
    )]
    #>

    Param(

        # Mail Client Setup
        [Alias('Server')]
        [Parameter(
            Mandatory=$true
        )]
        [ValidateNotNullOrEmpty()]
        [string]
        $MailServer,

        [Alias('Port')]
        [Parameter(
            Mandatory=$true
        )]
        [ValidateNotNullOrEmpty()]
        [uint16]
        $MailServerPort,

        [Alias('SSL')]
        [switch]
        $EnableSsl,

        [Alias('User','UserName','LogIn','Credential')]
        [pscredential]
        $SmtpCredentials,

        [Alias('Sender','From')]
        [Parameter(
            Mandatory=$true
        )]
        [ValidateNotNullOrEmpty()]
        [string]
        $SenderEmailAddress,

        [Alias('ReplyTo')]
        [string]
        $ReplyToAddress,

        [Alias('Recipients','To')]
        [Parameter(
            Mandatory=$true
        )]
        [ValidateNotNullOrEmpty()]
        [string[]]
        $VisibleRecipientEmailAddresses,

        [Alias('CC')]
        [string[]]
        $VisibleCarbonCopyEmailAddresses,

        [Alias('BCC')]
        [string[]]
        $BlindCarbonCopyEmailAddresses,

        [string]
        $Subject,

        [Alias('Message')]
        [string]
        $Body,

        [Alias('HTML')]
        [switch]
        $BodyIsHtml,

        [string[]]
        $Attachments,

        [string]
        [ValidateSet(
            'low',
            'normal',
            'high'

        )]
        $Priority = 'normal'

    )

    Begin{}

    Process{
        # Datatype conversions

        # Convert Priority to Integer
        switch($Priority){

            'low' {
                $_priority = 1
                break
            }

            'normal' {
                $_priority = 0
                break
            }

            'high' {
                $_priority = 2
                break
            }

        }

        # SMTP Client Construction
        $_smtpClient = 
            New-Object System.Net.Mail.SmtpClient(
                $MailServer, 
                $MailServerPort
            )

        if ($EnableSsl.IsPresent){

            $_smtpClient.EnableSsl = $true

        }

        $_smtpClient.Credentials = 
            New-Object System.Net.NetworkCredential(

                $SmtpCredentials.GetNetworkCredential().UserName,
                $SmtpCredentials.GetNetworkCredential().Password

            )

        # Email Message Construction
        $_email = 
            New-Object System.Net.Mail.MailMessage

        $_email.From        = $SenderEmailAddress
        
        if ($ReplyToAddress -ne "" -and $ReplyToAddress -ne $null){
            $_email.ReplyTo     = $ReplyToAddress
        }

        $_email.Subject     = $Subject
        $_email.Body        = $Body
        if ($BodyIsHtml.IsPresent){$_email.IsBodyHtml=$true}
        $_email.Priority    = $_priority

        foreach (
            $_visibleRecipientEmailAddress in 
            $VisibleRecipientEmailAddresses
        ){

            $_email.To.add($_visibleRecipientEmailAddress)
        }

        foreach (
            $_visibleCarbonCopyEmailAddress in 
            $VisibleCarbonCopyEmailAddresses
        ){
            $_email.CC.add($_visibleCarbonCopyEmailAddress)
        }

        foreach (
            $_blindCarbonCopyEmailAddress in 
            $BlindCarbonCopyEmailAddresses
        ){
            $_email.Bcc.add($_blindCarbonCopyEmailAddress)
        }
    
        foreach (
            $_attachment in 
            $Attachments
        ){
            $_email.Attachments.Add((
                New-Object System.Net.Mail.Attachment($_attachment)
            ))
        }
    
        # ACTION!!!
        $_smtpClient.Send($_email)
    }

    End {}
}