# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# Copyright 2019 Ryan James Decker

function Edit-Profile {
<#

#>
    [CmdletBinding(DefaultParameterSetName = 'Force')]

    Param(
        [Parameter(ParameterSetName='Interactive')]
        [switch]$Interactive,
    
        [Parameter(ParameterSetName='Force')]
        [switch]$Force
    )

    Begin {}

    Process {
    
        $_profileExists = Test-Path $profile

        if ($_profileExists) {

            POWERSHELL_ISE.EXE $profile

        } else {

            Write-Warning "Profile does not exist: `"$profile`""
        
            if ($Interactive.IsPresent) {
            
                switch(

                Get-SelectionFromMenu `
                    -Message "Do you want to create a new profile here: `"$profile`"?" `
                    -Options @("Yes","No")

                ) {
                    1 {
                        New-Profile -Verbose
                        break
                    }

                    2 {
                        Write-Verbose
                        break
                    }

                    default {

                        break
                    }
                }
        
            } elseif ($Force.IsPresent) {
            
                New-Profile -Verbose
                POWERSHELL_ISE.EXE $profile

            } else {

                Write-Error "Profile does not exist: `"$profile`""

            }

        
        }

    }

    End {}
}

function Get-Profile {

    [CmdletBinding()]

    Param()

    Begin {

        $_profileExists = Test-Path $profile

        if ($_profileExists) {
            $_profileFile = (ls $profile | select PsChildName).PsChildName
            $_profileFullPath = $profile
            $_profileParentDirectory = (ls $profile | select DirectoryName).DirectoryName
        } else {
            Write-Error "File not found: $profile"
            return
        }

        Write-Verbose "Creating profile object."
        $_profileObject = 
            New-Object -TypeName PsObject

        Write-Verbose "Setting profile object filename."
        $_profileObject | 
            Add-Member `
                -MemberType NoteProperty `
                -Name "ProfileFile" `
                -Value $_profileFile

        Write-Verbose "Setting full profile object path: `"$profile`""
        $_profileObject | 
            Add-Member `
                -MemberType NoteProperty `
                -Name "ProfileFullPath" `
                -Value $_profileFullPath
    
        Write-Verbose "Setting profile object directory."
        $_profileObject | 
            Add-Member `
                -MemberType NoteProperty `
                -Name "ProfileParentDirectory" `
                -Value $_profileParentDirectory
    }

    Process {}
    
    
    End {

        Write-Verbose "Getting profile object."
        return $_profileObject

    }

}

function New-Profile {
<#

#>
    [CmdletBinding()]

    Param()

    Begin {}

    Process {
    
        $_profileExists = test-path $profile

        if ($_profileExists) {

            Write-Error "Profile already exists: `"$profile`""

        } else {

            Write-Verbose "Attempting to create file: `"$profile`""
            New-Item -ItemType File -Path $profile -Force
            if ($?) {Write-Verbose "File created: `"$profile`""}
        }

    }

    End {}
}

function Invoke-Profile {

    [CmdletBinding()]

    Param()
    
    DynamicParam{
        
        $_nl = [System.Environment]::NewLine
        # Create the dynamic parameter name, dictionary, attribute collection
        $_dynamicParameterName = 'PowerShellProfile'
        $_runtimeParameterDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
        $_attributeCollection = New-Object System.Collections.ObjectModel.Collection[System.Attribute]
            
        # Create and set the parameters' attributes
        $_parameterAttribute = New-Object System.Management.Automation.ParameterAttribute
        $_parameterAttribute.Mandatory = $true

        # Create, generate, and set the ValidateSet 
        $_validateSet = @(
            $profile.CurrentUserAllHosts,
            $profile.CurrentUserCurrentHost
            $profile.AllUsersAllHosts,
            $profile.AllUsersCurrentHost
        )
        
        $_validateSetAttribute = New-Object System.Management.Automation.ValidateSetAttribute($_validateSet)
        
        # Create parameter attribute: help message
        $_helpMessage = 
            "$_nl`Enter one of the following:$_nl$_nl" + 
            ($_validateSet -join $_nl) + $_nl

        $_parameterAttribute.HelpMessage = $_helpMessage

        # Add the Parameter and ValidateSet attributes to the attributes collection
        $_attributeCollection.Add($_parameterAttribute)
        $_attributeCollection.Add($_validateSetAttribute)

        # Create the dynamic parameter
        $_runtimeParameter = New-Object System.Management.Automation.RuntimeDefinedParameter(
            $_dynamicParameterName,
            [string],
            $_attributeCollection
        )

        # Add the dynamic parameter to a collection and return them
        $_runtimeParameterDictionary.Add(
            $_dynamicParameterName,
            $_runtimeParameter
        )

        return $_runtimeParameterDictionary
        
    }

    begin {
        
        # Bind the parameter to a friendly variable
        $PowerShellProfile = $PSBoundParameters[$_dynamicParameterName]

    }

    process {
    
        Write-Verbose "Invoking profile: $PowerShellProfile"
        
        & $PowerShellProfile

    }

    end {}
    
}