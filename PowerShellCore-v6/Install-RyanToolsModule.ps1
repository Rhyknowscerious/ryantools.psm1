<# This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at https://mozilla.org/MPL/2.0/.

 # Copyright 2019 Ryan James Decker
        
        .SYNOPSIS
        Updates the modules Ryan Tools and Ryan Tools Admin.

        .DESCRIPTION
        This command will update the modules Ryan Tools and Ryan Tools Admin 
        from a public Bitbucket repository at: 
        
        https://bitbucket.org/Rhyknowscerious/ryan_tools 

        Basically it will download, extract, and move these modules to an 
        appropriate location for PowerShell to automatically load the module 
        upon use.

        If you want to install the modules yourself, just download the modules and put 
        them in the appropriate directories

        .PARAMETER Force
        This switch will enable over-writing files and automatically creating necessary folders if missing.

        .PARAMETER NoCleanup
        Normally, this command will delete the downloaded archive and extracted temporary files. Use this switch to keep these files instead.

        .PARAMETER ModuleArchiveDownloadUrl
        This is where the module zip file is on BitBucket.

        .PARAMETER ModuleArchiveDownloadFilePath
        This is where the downloaded zip file will be saved before extraction.

        .PARAMETER ModuleExtractionDirectory
        This is where the modules will be stored temporarily before extraction.

        .PARAMETER ModuleInstallDirectory
        This tells the command where to install the updated module.

        .INPUTS
        None

        .OUTPUTS
        None
    #>

    [CmdletBinding()]

    Param(
            
        [switch]$Force,

        [switch]$NoCleanup,
        
        $ModuleArchiveDownloadUrl,

        $ModuleArchiveDownloadFilePath = 
            "$HOME\Downloads\RyanToolsModuleArchive.zip",

        $ModuleExtractionDirectory = 
            "$HOME\Downloads\RyanToolsModuleArchiveExtracted\"
    )

    DynamicParam {
           
        $_nl = [System.Environment]::NewLine
        # Create the dynamic parameter name, dictionary, attribute collection
        $_dynamicParameterName = 'ModuleInstallDirectory'
        $_runtimeParameterDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
        $_attributeCollection = New-Object System.Collections.ObjectModel.Collection[System.Attribute]
            
        # Create and set the parameters' attributes
        $_parameterAttribute = New-Object System.Management.Automation.ParameterAttribute
        $_parameterAttribute.Mandatory = $true

        # Create, generate, and set the ValidateSet 
        $_validateSet = $env:PSModulePath -split ';'
        #$_validateSet += "$HOME\Downloads\PowerShellModules"
        #$_validateSetAttribute = New-Object System.Management.Automation.ValidateSetAttribute($_validateSet)
        
        # Create parameter attribute: help message
        $_helpMessage = 
            "$_nl`Here are configured powershell module installation " + 
            "locations on for this computer but you can choose whatever you " + 
            "want:$_nl$_nl" + 
            ($_validateSet -join $_nl) + $_nl+$_nl + 
            "You should usually put modules for specific users to access in " + 
            "`"`$HOME\Documents\WindowsPowerShell`". Modules for all users " + 
            "typically go into `"`$env:ProgramFiles\WindowsPowerShell\Modules`" " + 
            "or `"`${env:ProgramFiles(x86)}\WindowsPowerShell\Modules`" " + 
            "And system modules typically go in `"`$PSHOME\Modules`"."+$_nl+$_nl

        $_parameterAttribute.HelpMessage = $_helpMessage

        # Add the Parameter and ValidateSet attributes to the attributes collection
        $_attributeCollection.Add($_parameterAttribute)
        $_attributeCollection.Add($_validateSetAttribute)

        # Create the dynamic parameter
        $_runtimeParameter = New-Object System.Management.Automation.RuntimeDefinedParameter(
            $_dynamicParameterName,
            [string],
            $_attributeCollection
        )

        # Add the dynamic parameter to a collection and return them
        $_runtimeParameterDictionary.Add(
            $_dynamicParameterName,
            $_runtimeParameter
        )

        return $_runtimeParameterDictionary
        
    }

    begin {
        
        # Bind the parameter to a friendly variable
        $ModuleInstallDirectory = $PsBoundParameters[$_dynamicParameterName]

        $_verbose = @{}

        $_force = @{}

        if (

            $PSCmdlet.MyInvocation.BoundParameters["Verbose"].IsPresent

        ) {

            $_verbose = @{Verbose=$true}

        }

        if ($Force.IsPresent) { $_force = @{Force=$true} }

        if (!$ModuleArchiveDownloadUrl) {

            [Net.ServicePointManager]::SecurityProtocol = 
                [Net.SecurityProtocolType]::Tls12

            $_html = 
                Invoke-WebRequest `
                    -Uri "https://bitbucket.org/Rhyknowscerious/ryan_tools/downloads/" `
                    @_verbose

            $_htmlAsArray = 
                $_html.RawContent.Split([System.Environment]::NewLine)

            $_htmlTargetLine = $_htmlAsArray | sls '\.zip'
            
            $_url = "https://www.bitbucket.org"

            $_url += $_htmlTargetLine.ToString().trim() -replace 
                '^.*href="','' -replace 
                '">Download reposit.*$',''

            $ModuleArchiveDownloadUrl = $_url

        }
    }

    process{

        $_updateStartTime = Get-Date

        [Net.ServicePointManager]::SecurityProtocol = 
            [Net.SecurityProtocolType]::Tls12

        $_webClient = New-Object System.Net.WebClient @_verbose

        Write-Verbose "Attempting to download module `"$ModuleArchiveDownloadUrl`" to `"$ModuleArchiveDownloadFilePath`". Please wait...$_nl$_nl"

        $_downloadStartTime = Get-Date

        $_webClient.DownloadFile(
            $ModuleArchiveDownloadUrl, 
            $ModuleArchiveDownloadFilePath
        )
        
        $_downloadSucceeded = $?

        $_downloadEndTime = Get-Date

        if ($_downloadSucceeded) {

            Write-Verbose "Module downloaded from `"$ModuleArchiveDownloadUrl`" saved: `"$ModuleArchiveDownloadFilePath`"$_nl$_nl"
        
        } else {
        
            Write-Error "Module update failed. Download failed: `"$ModuleArchiveDownloadUrl`""

            return
        
        }
    
        $_downloadTimeSpan = New-TimeSpan $_downloadStartTime $_downloadEndTime

        Write-Verbose "Download duration: $_downloadTimeSpan$_nl$_nl"

        Write-Verbose "Extracting $ModuleArchiveDownloadFilePath to `"$ModuleExtractionDirectory`"...$_nl$_nl"

        Expand-Archive `
            -Path $ModuleArchiveDownloadFilePath `
            -DestinationPath "$ModuleExtractionDirectory" `
            @_force `
            @_verbose

        $_extractedSuccessfully = $?

        if ($_extractedSuccessfully) {
        
            Write-Verbose "Extraction successful.$_nl$_nl"

        } else {
        
            Write-Error "Module update failed. Extraction failed: `"$ModuleArchiveDownloadFilePath`""

            return

        }

        $_updatedModulesFolder = (
            Get-ChildItem $ModuleExtractionDirectory
        ).FullName

        $_updatedModules = (
            Get-ChildItem $_updatedModulesFolder -Directory
        ).FullName

        Write-Verbose "Copying modules from `"$_updatedModulesFolder`" to `"$ModuleInstallDirectory...$_nl$_nl"
        
        if (!(Test-Path $ModuleInstallDirectory)) {
            
            Write-Verbose "Folder doesn't exist `"$ModuleInstallDirectory`"$_nl$_nl"

            New-Item `
                -ItemType Directory `
                -Path $ModuleInstallDirectory `
                @_verbose

            if (!$?) {

                Write-Error "Could not create directory $"

                return
            }
        }
            
        Copy-Item `
            -Path $_updatedModules `
            -Destination $ModuleInstallDirectory `
            -Recurse `
            -Container `
            @_verbose `
            @_force

        $_filesCopiedSuccesfully = $?

        if ($_filesCopiedSuccesfully) {
            
            $_updateEndTime = Get-Date

            $_updateTimeSpan = New-TimeSpan $_updateStartTime $_updateEndTime

        } else {

            Write-Error "Module update failed: Error copying files from `"$_updatedModulesFolder`" to `"$ModuleInstallDirectory`"."
            return
        }

        Write-Verbose "Module updated successfully. Update duration: $_updateTimeSpan$_nl$_nl"
        
        if ($NoCleanup.IsPresent) {
            
            Write-Verbose "Temporary files NOT removed: $_nl`t$ModuleArchiveDownloadUrl$_nl`t$ModuleExtractionDirectory$_nl$_nl"

        } else {

            Write-Verbose "Cleaning up downloaded files.$_nl$_nl"

            Remove-Item `
                -Path $ModuleArchiveDownloadFilePath,$ModuleExtractionDirectory `
                -Recurse `
                @_force `
                @_verbose

            $_tempFile = ls $ModuleInstallDirectory -Filter "*Rhyknowscerious*"

            if ($_tempFile.FullName -match "Rhyknowscerious") {
                
                Remove-Item `
                    -Path $_tempFile.FullName `
                    -Recurse `
                    @_force `
                    @_verbose

            }
        }
    }

    End {
        
    }