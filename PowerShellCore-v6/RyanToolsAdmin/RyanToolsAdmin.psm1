# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# Copyright 2019 Ryan James Decker

#Requires -RunAsAdministrator

function Restart-NetworkAdapter {

    <##>
    
    Param()

    Write-Host "Choose a network adapter to toggle on/off."

    $_networkAdapters = Get-NetAdapter

    [uint16]$_choice = 
        Get-SelectionFromMenu `
            -Message "These network adapters were found on your system." `
            -Options $_networkAdapters.Name

    if (

        $_choice -lt 1 -or 
        $_choice -ge $_networkAdapters.length

    ) {
    
        Throw {"Invalid selection"}
    }

    $_selectedNetworkAdapter = $_networkAdapters[$_choice-1]
    
    Restart-NetAdapter $_selectedNetworkAdapter.Name

}

function Set-TrustedHosts {

    <#
        .SYNOPSIS
        Simple command that sets the trusted hosts configuration in WsMan

        .DESCRIPTION
        A wrapper for Set-Item -Path WSMan:\localhost\Client\TrustedHosts -Value [string]

        .INPUTS
        [string], [string[]], or [Microsoft.WSMan.Management.WSManConfigElement]

        .OUTPUTS
        None
    #>

    [CmdletBinding()]

    Param(

        [Parameter(
            Mandatory=$true
        )]
        $TrustedHosts,

        [switch]$Append

    )

    Begin{
        
        if (

            $TrustedHosts.GetType() -eq [string]

        ) {

            $_trustedHosts = $TrustedHosts
            
        } elseif (

            $TrustedHosts.GetType() -eq [object[]] -and 
            $TrustedHosts[0].GetType() -eq [string]

        ) {

            $_trustedHosts = $TrustedHosts -join ','
        
        } elseif (

            $TrustedHosts.gettype() -eq [Microsoft.WSMan.Management.WSManConfigLeafElement]

        ){

            $_trustedHosts = ($TrustedHosts).Value
        
        } else {
            
            $_inputObjectType = $TrustedHosts.GetType().Name
            $_message = 
                "You've provided an object of the type [$_inputObjectType]. " + 
                "This command only supports [string], [string[]], and [WSManConfigLeafElement]"

            Write-Error $_message
            return

        }
    }

    Process{
        
        if (

            $Append.IsPresent -and 
            (Get-Item WSMan:\localhost\Client\TrustedHosts).value -ne ''

        ) {

            $_trustedHosts += ',' + (Get-Item WSMan:\localhost\Client\TrustedHosts).value

        }

    }

    End{

        Set-Item WSMan:\localhost\Client\TrustedHosts -Value $_trustedHosts

        Get-Item WSMan:\localhost\Client\TrustedHosts

        return

    }

}

function Toggle-Ipv6 {
    <#
        .SYNOPSIS
        Toggle-Ipv6 will disable IPv6 on a network interface card.

        .DESCRIPTION
        Enable or disable IPv6 on a NIC using WMI under the hood.

        .PARAMETER Enable
        Enables the network adapter

        .PARAMETER Disable
        Disables the network adapter

        .PARAMETER ComputerName
        Here's where you specify computers to run this on. If omitted, runs locally.

        .PARAMETER Credential
        Enter "username" or "userdomain\username" to specify a different user.

        .PARAMETER NetConnectionId
        The name of the network interface card that you wish to toggle.

        .PARAMETER NetworkAdapterFilterScript
        Use this to filter out the Win32_NetworkAdapter WMI class using Where-Object's -FilterScript parameter.

    #>

    [CmdletBinding()]

    Param(
        
        [parameter(
            ValueFromPipeline=$true
        )]
        $ComputerName,
            
        $Credential,

        [switch]$Force

    )

    DynamicParam{

        $_nl = [System.Environment]::NewLine
        
        # Construct 
        $_runtimeParameterDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
        $_parameterAttribute = New-Object System.Management.Automation.ParameterAttribute
        $_attributeCollection = New-Object System.Collections.ObjectModel.Collection[System.Attribute]

        # Set Dynamic Parameter Properties
        $_runtimeParameterName = "NetConnectionId"
        $_runtimeParameterType = [string]
        
        # Construct ValidateSet Attribute
        $_networkAdapters =
            Get-WmiObject `
                -Class Win32_NetworkAdapter `
                -Filter 'NetConnectionId != NULL'

        $_validateSet = 
            $_networkAdapters.NetConnectionId

        $_validateSetAttribute = 
            New-Object System.Management.Automation.ValidateSetAttribute(
                $_validateSet
            )

        # Set Parameter Attribute Properties
        $_parameterAttribute.Mandatory = $true
        $_parameterAttribute.HelpMessage = "You must enter one of the following NetConnectionId`'s:$_nl$($_validateSet -join "$_nl")"

        # Add attrubutes attribute collection
        $_attributeCollection.Add($_parameterAttribute)
        $_attributeCollection.Add($_validateSetAttribute)
 
        # Construct the dynamic parameter
        $_runtimeParameter = 
            New-Object System.Management.Automation.RuntimeDefinedParameter(
                $_runtimeParameterName,
                $_runtimeParameterType,
                $_attributeCollection
            )
 
        # Add dynamic parameter to the parameter Dictionary
        $_runtimeParameterDictionary.Add($_runtimeParameterName, $_runtimeParameter)
        
        return $_runtimeParameterDictionary

    }

    Begin{
        
        $NetConnectionId = $PSBoundParameters['NetConnectionId']
        
        $_netAdapterBindingArgs = @{
            
            Name = $NetConnectionId
            ComponentId = "ms_tcpip6"

        }

    }

    Process{

        $_nl = [System.Environment]::NewLine

        Write-Verbose "Checking for existing IPv6 status of NetConnectionId `"$NetConnectionId`"..."
        $_netAdapterBindingOld = Get-NetAdapterBinding @_netAdapterBindingArgs
        $_ipv6IsEnabled = $_netAdapterBindingOld.Enabled
        
        Write-Host "Current IPv6 status for $NetConnectionId`:"
        $_netAdapterBindingOld

        if ($Force.IsPresent) {
            
            $_force = @{Confirm = $false}

        } else {

            $_force = @{Confirm = $true}

        }

        if ($_ipv6IsEnabled) {
            
            Disable-NetAdapterBinding @_netAdapterBindingArgs @_force -Verbose

        } else {
            
            Enable-NetAdapterBinding @_netAdapterBindingArgs @_force -Verbose

        }

        Write-Verbose "Checking for updated IPv6 status of NetConnectionId `"$NetConnectionId`":"
        $_netAdapterBindingNew = Get-NetAdapterBinding @_netAdapterBindingArgs

        if ($_netAdapterBindingOld.Enabled -ne $_netAdapterBindingNew.Enabled) {
        
            Write-Host "$_nl`Updated IPv6 status for $NetConnectionId`:"
            $_netAdapterBindingNew

        }

    }

    End{}
}

function Toggle-NetworkAdapter {
    Param()

    Write-Host "Choose a network adapter to toggle on/off."

    $_networkAdapters = Get-NetAdapter

    [uint16]$_choice = 
        Get-SelectionFromMenu `
            -Message "These network adapters were found on your system." `
            -Options $_networkAdapters.Name

    if (

        $_choice -lt 1 -or 
        $_choice -ge $_networkAdapters.length

    ) {
    
        Throw {"Invalid selection"}
    }

    $_selectedNetworkAdapter = $_networkAdapters[$_choice-1] 

    if ($_selectedNetworkAdapter.AdminStatus -ne 'up') {
    
        Enable-NetAdapter $_selectedNetworkAdapter.Name

    } elseif ($_selectedNetworkAdapter.AdminStatus -ne 'down') {

        Disable-NetAdapter $_selectedNetworkAdapter.Name

    }

}

function Disable-WindowsRecoveryBootScreen {

    <#
    
        .SYNOPSIS
        Disable the Windows recovery screen at boot.

        .DESCRIPTION
        With this command you can disable the recovery screen at Windows boot time.
        
        It basically calls this CMD.EXE command via PowerShell's Invoke-Command Cmdlet:

            BCDEDIT -SET BootStatusPolicy IgnoreAllFailures

        .PARAMETER Computername
        Pass a ComputerName argument to Invoke-Command

        .PARAMETER Credential
        Pass a Credential argument to Invoke-Command

        .INPUTS
        None

        .OUTPUTS
        None

        .EXAMPLE
        Disable-WindowsRecoveryBootScreen -ComputerName RemoteComputer1,RemoteComputer2
    
        .LINK
        https://community.spiceworks.com/how_to/43975-turn-off-windows-error-recovery-screen

    #>

    [CmdletBinding()]

    Param(

        [Parameter(Mandatory=$true)]
        $ComputerName,

        $Credential

    )

    Begin{
    
        $_icmArgs = @{}    
    
        if ($ComputerName) {
        
            $_icmArgs += @{ComputerName = $ComputerName}

        }

        if ($Credential) {
        
            $_icmArgs += @{Credential = $Credential}

        }

        $_icmArgs += @{

            ScriptBlock = {
                
                $VerbosePreference = "Continue"

                Write-Verbose "$env:COMPUTERNAME.$env:USERDOMAIN: Disabling boot recovery screen..."
        
                BCDEDIT -SET BootStatusPolicy IgnoreAllFailures

            }

        }

        if ($PSCmdlet.MyInvocation.BoundParameters["Verbose"]) {
            
            $_icmArgs += @{Verbose = $true}

        }

    }

    Process{

        Invoke-Command @_icmArgs

    }

    End{}

}


function Enable-WindowsRecoveryBootScreen {

    <#
    
        .SYNOPSIS
        Enable the Windows error recovery screen at boot.

        .DESCRIPTION
        With this command you can enable the recovery screen at Windows boot time.
        
        It basically calls this CMD.EXE command via PowerShell's Invoke-Command Cmdlet:

            BCDEDIT -SET BootStatusPolicy DisplayAllFailures

        .PARAMETER Computername
        Pass a ComputerName argument to Invoke-Command

        .PARAMETER Credential
        Pass a Credential argument to Invoke-Command

        .INPUTS
        None

        .OUTPUTS
        None

        .EXAMPLE
        Enable-WindowsRecoveryBootScreen -ComputerName RemoteComputer1,RemoteComputer2

        .LINK
        https://community.spiceworks.com/how_to/43975-turn-off-windows-error-recovery-screen

    #>

    [CmdletBinding()]

    Param(

        [Parameter(Mandatory=$true)]
        $ComputerName,

        $Credential
    
    )

    Begin{

        $_icmArgs = @{}    
    
        if ($ComputerName) {
        
            $_icmArgs += @{ComputerName = $ComputerName}

        }

        if ($Credential) {
        
            $_icmArgs += @{Credential = $Credential}

        }

        $_icmArgs += @{ 
            
            ScriptBlock = {
                
                $VerbosePreference = "Continue"

                Write-Verbose "$env:COMPUTERNAME.$env:USERDOMAIN: Enabling boot recovery screen..."

                BCDEDIT -SET BootStatusPolicy DisplayAllFailures

            }
        }

        if ($PSCmdlet.MyInvocation.BoundParameters["Verbose"]) {
            
            $_icmArgs += @{Verbose = $true}

        }
    }

    Process{

        Invoke-Command @_icmArgs

    }

    End{}

}